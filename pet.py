# elaborar um código de herança para armazenar de forma persistente as classes Gato (nome, raça, cor, genero e fugas) e Cachorro (nome, raça, cor e genero); cadastrar o gato Merlin e o cachorro Bilu. Dica: você deverá criar uma classe Animal :-)

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
path = os.path.dirname(os.path.abspath(__file__))
arquivobd = os.path.join(path, 'profissoes.db')
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///"+arquivobd
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # remover warnings
db = SQLAlchemy(app)

# definições das classes
class Animal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    idade = db.Column(db.String(254))
    raça = db.Column(db.String(254))
    genero = db.Column(db.String(254))

    # atributo necessário para armazenar tipo de classe especializada (discriminador)
    type = db.Column(db.String(50))
    
    # definições de mapeamento da classe mãe
    __mapper_args__ = {
        'polymorphic_identity':'animal', 
        'polymorphic_on':type # nome do campo que da a especie dos animais
    }

    def __str__(self):
        return f'{self.nome}, {self.idade}, {self.raça}, {self.genero}'
    
class Gato(Animal):
    # se o campo abaixo for removido, uma única tabela será criada
    id = db.Column(db.Integer, db.ForeignKey('animal.id'), primary_key=True)

    # a identidade polimórfica da classe será armazenada 
    # no campo type da classe pai
    __mapper_args__ = { 
        'polymorphic_identity':'gato',
    }
    caixa_de_areia = db.Column(db.Integer)

    def __str__(self):
        return super().__str__() + f", caixa_de_areia ={self.caixa_de_areia}"


class Cachorro(Animal):
    id = db.Column(db.Integer, db.ForeignKey('animal.id'), primary_key=True)

    __mapper_args__ = { 
        'polymorphic_identity':'cachorro',       
    }

    tapete_higi = db.Column(db.String(254))

    def __str__(self):
            return f'{super().__str__()}, tapete higienico: {self.tapete_higi}'

with app.app_context():

    # início do programa de testes
    if os.path.exists(arquivobd): # se o arquivo já existe...
        os.remove(arquivobd) # ... o arquivo é removido

    db.create_all() # criar as tabelas no banco


marlin = Gato(nome="marlin", idade="5", raça="laranja", genero="macho", caixa_de_areia="sim")

bidu = Cachorro(nome="bidu", idade="7", raça="husky", genero="macho", tapete_higi="não possui")

print(f'Gato:{marlin},\nCachorro:{bidu}')